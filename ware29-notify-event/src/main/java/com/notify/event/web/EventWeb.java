package com.notify.event.web;

import com.notify.event.service.EventService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 公众号:知了一笑
 * @since 2022-04-23 18:11
 */
@RestController
public class EventWeb {

    @Resource
    private EventService eventService ;

    @GetMapping("event/msg")
    public String msg (){
        eventService.changeState(220423999,1,2);
        return "sus" ;
    }

}
